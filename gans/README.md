TODO
* add tensorboard
* improve output show (table of images)
* run on more complicated dataset - eg. CelebA

Useful links:
* https://blog.paperspace.com/implementing-gans-in-tensorflow/
* https://towardsdatascience.com/having-fun-with-deep-convolutional-gans-f4f8393686ed
* https://medium.com/coinmonks/celebrity-face-generation-using-gans-tensorflow-implementation-eaa2001eef86