from functools import reduce
from operator import mul

import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.python.keras import Model, Sequential
from tensorflow.python.keras.layers import Input, Dense, BatchNormalization, Activation, LeakyReLU, ReLU, Reshape, \
    Conv2DTranspose, Conv2D, Flatten
from tensorflow.python.keras.optimizer_v2.adam import Adam

from gans import datasets

print(tf.VERSION)
print(tf.keras.__version__)

# Generator:
NOISE_DIM = 100

# Discriminator:
DISCRIMINATOR_INPUT = (None, 28, 28)
DISCRIMINATOR_ALPHA = 0.2
DISCRIMINATOR_LR = 0.0001
DISCRIMINATOR_STEPS = 2

# gans
GAN_LR = 0.0001

# TRAINING
EPOCHS = 100
BATCH_SIZE = 512


def create_generator() -> Model:
    first_conv_shape = (7, 7, 256)

    model = Sequential([
        # batch size not included in input shape:
        Dense(reduce(mul, first_conv_shape), use_bias=False, input_shape=(NOISE_DIM,), name='gen_dense'),
        BatchNormalization(name='gen_batch_norm_1'),
        ReLU(name='gen_relu_1'),
        Reshape(first_conv_shape, name='gen_reshape'),

        Conv2DTranspose(filters=64, kernel_size=(5, 5), strides=(1, 1), padding='same', use_bias=False,
                        name='gen_conv_1'),
        BatchNormalization(name='gen_batch_norm_2'),
        ReLU(name='gen_relu_2'),

        Conv2DTranspose(filters=32, kernel_size=(5, 5), strides=(2, 2), padding='same', use_bias=False,
                        name='gen_conv_2'),
        BatchNormalization(name='gen_batch_norm_3'),
        ReLU(name='gen_relu_3'),

        Conv2DTranspose(filters=1, kernel_size=(5, 5), strides=(2, 2), padding='same', use_bias=False,
                        name='gen_conv_3'),
        # in DCGAN paper they're claiming removing batch norm from generators output layer improves stability
        Activation('tanh', name='gen_output')
    ])

    inputs = Input(shape=(NOISE_DIM,))
    generator = model(inputs)

    return Model(inputs, generator)  # TODO jan: find out why this is wrapped this way


def create_discriminator() -> Model:
    discriminator_input_shape = (28, 28, 1)

    model = Sequential([
        # batch size not included in input shape:
        Conv2D(filters=32, kernel_size=(5, 5), strides=(2, 2), padding='same',
               input_shape=discriminator_input_shape, name='disc_conv_1'),
        # in DCGAN paper they're claiming removing batch norm from discriminator input layer improves stability
        LeakyReLU(alpha=DISCRIMINATOR_ALPHA, name='disc_relu_1'),

        Conv2D(filters=16, kernel_size=(5, 5), strides=(2, 2), padding='same', name='disc_conv_2'),
        BatchNormalization(name='disc_batch_norm_1'),
        LeakyReLU(alpha=DISCRIMINATOR_ALPHA, name='disc_relu_2'),

        Flatten(name='disc_flatten'),
        Dense(784, name='disc_dense_1'),
        BatchNormalization(name='disc_batch_norm_2'),
        LeakyReLU(alpha=DISCRIMINATOR_ALPHA, name='disc_relu_3'),

        Dense(1, name='disc_dense_2'),
        Activation('sigmoid', name='disc_output')
    ])

    inputs = Input(shape=discriminator_input_shape)
    discriminator = model(inputs)

    return Model(inputs, discriminator)


def create_gan(discriminator: Model, generator: Model):
    inputs = Input(shape=(NOISE_DIM,))
    discriminator.trainable = False

    gan = discriminator(generator(inputs))
    return Model(inputs, gan)


def sample_noise(size: int = BATCH_SIZE):
    return np.random.normal(loc=0, scale=1, size=(size, NOISE_DIM))


def show_img(x):
    x = (x / 2 + 1) * 255
    x = np.clip(x, 0, 255)
    x = np.uint8(x)
    x = x.reshape(28, 28)

    plt.imshow(x, cmap='gray')
    plt.show()


def train_gan_using_dataset_api():
    # models:
    discriminator = create_discriminator()
    discriminator.compile(optimizer=Adam(learning_rate=DISCRIMINATOR_LR),
                          loss='binary_crossentropy',
                          metrics=['accuracy'])
    discriminator.summary()

    generator = create_generator()
    generator.summary()

    gan = create_gan(discriminator, generator)
    gan.compile(optimizer=Adam(learning_rate=GAN_LR),
                loss='binary_crossentropy')
    # metrics=['accuracy']) NOTE: adding accuracy causes problem with training GAN - chaining models fails
    gan.summary()

    # datasets
    imgs = datasets.prepare_mnist_dataset(BATCH_SIZE, prefetch_multiplier=1000)
    imgs_it = imgs.make_initializable_iterator()
    fake_labels = np.zeros((BATCH_SIZE, 1))
    valid_label = np.ones((BATCH_SIZE, 1))

    # train
    with tf.Session() as sess:
        img_input, img_labels = imgs_it.get_next()

        for epoch in range(EPOCHS):
            sess.run([imgs_it.initializer])

            count = 0
            d_loss = 0.0
            d_accuracy = 0.0
            g_loss = 0.0
            g_accuracy = 0.0

            while True:
                try:
                    noise = sample_noise()
                    count += 1

                    # train discriminator
                    real_loss, real_accuracy = discriminator.train_on_batch(x=img_input, y=img_labels)
                    d_loss += real_loss
                    d_accuracy += real_accuracy

                    gen_imgs = generator.predict(noise)
                    fake_loss, fake_accuracy = discriminator.train_on_batch(x=gen_imgs, y=fake_labels)
                    d_loss += fake_loss
                    d_accuracy += fake_accuracy

                    # train generator
                    gan_loss = gan.train_on_batch(noise, valid_label)
                    g_loss += gan_loss

                    print('\t\tDisc: {}'.format((real_loss + fake_loss) / 2))
                    print('\t\tGen:  {}'.format(gan_loss))
                except tf.errors.OutOfRangeError:
                    break
            print('Discriminator loss: {} \t accuracy: {}'.format(d_loss / (2 * count), d_accuracy / (2 * count)))
            print('Generator loss: {} \t accuracy: {}'.format(g_loss / count, g_accuracy / count))
            show_img(generator.predict_on_batch(sample_noise(1)))


def train_gan_from_np_array():
    # models:
    discriminator = create_discriminator()
    discriminator.compile(optimizer=Adam(learning_rate=DISCRIMINATOR_LR),
                          loss='binary_crossentropy')

    generator = create_generator()

    gan = create_gan(discriminator, generator)
    gan.compile(optimizer=Adam(learning_rate=GAN_LR),
                loss='binary_crossentropy')

    # dataset
    (x_train, _), (_, _) = tf.keras.datasets.mnist.load_data()
    # normalize and reshape data:
    x_train = np.asarray((x_train - 127.5) / 127.5, dtype=np.float).reshape((-1, 28, 28, 1))
    print(x_train.shape)

    y_real = np.ones(BATCH_SIZE)
    y_fake = np.zeros(BATCH_SIZE)

    for epoch in range(EPOCHS):
        np.random.shuffle(x_train)

        count = 0
        d_loss = 0.0
        g_loss = 0.0

        for i in range(len(x_train) // BATCH_SIZE):
            noise = sample_noise()
            count += 1

            # train discriminator
            real_batch = x_train[i * BATCH_SIZE:(i + 1) * BATCH_SIZE]
            real_loss = discriminator.train_on_batch(x=real_batch, y=y_real)
            d_loss += real_loss

            fake_batch = generator.predict_on_batch(noise)
            fake_loss = discriminator.train_on_batch(x=fake_batch, y=y_fake)
            d_loss += fake_loss

            # train generator
            gan_loss = gan.train_on_batch(noise, y_real)
            g_loss += gan_loss

        print('Gan: {} \t\t Disc:{}'.format(g_loss, d_loss))
        generated_img = generator.predict_on_batch(sample_noise(1))
        show_img(generated_img)


if __name__ == '__main__':
    # using dataset api is slower for mnist
    train_gan_from_np_array()
