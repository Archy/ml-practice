import tensorflow as tf
import numpy as np


def prepare_mnist_dataset(batch_size: int, prefetch_multiplier: int = 10) -> tf.data.Dataset:
    (x_train, _), (_, _) = tf.keras.datasets.mnist.load_data()
    # normalize and reshape data:
    x_train = np.asarray((x_train - 127.5) / 127.5, dtype=np.float).reshape((-1, 28, 28, 1))
    y_real = np.ones(x_train.shape[0])

    mnist = tf.data.Dataset.from_tensor_slices((x_train, y_real))
    mnist = mnist.shuffle(buffer_size=batch_size*prefetch_multiplier)
    mnist = mnist.batch(batch_size=batch_size)
    mnist = mnist.prefetch(buffer_size=batch_size*prefetch_multiplier)

    return mnist


def prepare_noise_dataset(noise_size, batch_size: int, prefetch_multiplier: int = 10):
    def _generator():
        return np.random.normal(loc=0, scale=1, size=(noise_size,)), 0

    noise = tf.data.Dataset.from_generator(
        generator=_generator,
        output_types=(tf.float32, tf.int32),
        output_shapes=(tf.TensorShape([noise_size]), tf.TensorShape([1]))
    )
    noise = noise.shuffle(buffer_size=batch_size*prefetch_multiplier)
    noise = noise.batch(batch_size=batch_size)
    noise = noise.prefetch(buffer_size=prefetch_multiplier * batch_size)
    # noise = noise.apply(tf.data.experimental.shuffle_and_repeat(buffer_size=prefetch_multiplier * batch_size))

    return noise


if __name__ == '__main__':
    prepare_mnist_dataset(32)
    prepare_noise_dataset(10, 32)
