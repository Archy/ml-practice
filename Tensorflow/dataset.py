from typing import Tuple

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


STARTUPS_DATASET_PATH = r'C:\Users\Jan\Desktop\ML-practice\datasets\ks-projects-201801.csv'


def _load_startups_dataset(print_statistics: bool = False) -> pd.DataFrame:
    startups = pd.read_csv(STARTUPS_DATASET_PATH, dtype={
        'name': 'object',
        'category': 'category',
        'main_category': 'category',
        'currency': 'category',
        'goal': 'float64',
        'state': 'category',
        'backers': 'int64',
        'country': 'category',
        'usd pledged': 'float64',
        'usd_pledged_real': 'float64',
        'usd_goal_real': 'float64',
    }, parse_dates=['deadline', 'launched'])

    if print_statistics:
        print('Any values missing:', startups.isnull().values.any(), '\n\n')
        print('Missing values by column:\n', startups.isnull().sum(), '\n\n')
        print(startups.head())
        print('\n\nColumns types:')
        print(startups.dtypes)
        print('\n\nColumns statistics:')
        print(startups.describe(include=[np.object]))       # string columns
        print(startups.describe(include=[np.number]))       # numeric columns
        print(startups.describe(include=[np.datetime64]))   # time columns
        print(startups.describe(include=['category']))      # categorical columns

    return startups


def _parse_startups(startups: pd.DataFrame, random_state: int = None, print_statistics: bool = False) -> \
        Tuple[pd.DataFrame, pd.Series]:
    # drop columns
    columns_to_drop = [
        'ID', 'name',   # not needed
        'usd pledged',  # has missing values and is ~duplicated by usd_pledged_real
        'launched',     # lots of missing values(?)
        'deadline'      # ambiguous meaning
    ]
    startups = startups.drop(columns_to_drop, axis=1)
    # drop undefined, live and suspended states, as there's too little of them in the dataset:
    startups = startups.drop(startups[~startups['state'].isin(['failed', 'successful', 'canceled'])].index)
    startups['state'] = startups['state'].cat.remove_unused_categories()

    # startups = startups.drop(['category', 'main_category', 'currency', 'country'], axis=1)

    startups = startups.sort_values('backers')

    # https://stackoverflow.com/questions/48054704/typeerror-cannot-convert-value-dtypem8ns-to-a-tensorflow-dtype
    # startups = startups.reset_index(drop=True)

    target = startups['state']
    inputs = startups.drop('state', axis=1)

    if print_statistics:
        print('\n\nAfter cleanup:')
        print(inputs.columns.values)
        target.value_counts().plot(kind='bar')
        plt.show()

    # split to train and test
    return inputs, target


def prepare_startups_dataset(random_state: int = None, print_statistics: bool = False) -> \
        Tuple[pd.DataFrame, pd.Series]:
    startups = _load_startups_dataset(print_statistics)
    return _parse_startups(startups, random_state, print_statistics)


if __name__ == '__main__':
    pd.set_option('display.max_columns', None)  # don't truncate columns when printing
    prepare_startups_dataset(print_statistics=True)
