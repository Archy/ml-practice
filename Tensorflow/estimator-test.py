import logging
import os

import numpy as np
import tensorflow as tf
import tensorflow_estimator as tfe
from sklearn.model_selection import train_test_split

from Tensorflow import dataset

# Configuration:
MODEL_DIR = 'dnn_classfier.model_dir'
EPOCHS = 100
BATCH_SIZE = 32

RANDOM_SEED = 42


def prepare_startups_dataset():
    inputs, target = dataset.prepare_startups_dataset()

    numeric_columns = inputs.select_dtypes(include=[np.number]).columns.values
    categorical_columns = inputs.select_dtypes(include=['category']).columns.values

    # TensorFlow doesn't support categorical columns, so map them to string
    for column in categorical_columns:
        inputs[column] = inputs[column].astype(str)
    target = target.astype(str)

    # split to train and test
    X_train, X_test, y_train, y_test = train_test_split(inputs, target, test_size=0.33, random_state=RANDOM_SEED)

    numeric_features = [tf.feature_column.numeric_column(key=column) for column in numeric_columns]
    categorical_features = [
        tf.feature_column.indicator_column(
            tf.feature_column.categorical_column_with_vocabulary_list(key=column,
                                                                      dtype=None,
                                                                      vocabulary_list=list(inputs[column].unique())),
        )
        for column in categorical_columns
    ]

    input_features = numeric_features + categorical_features

    # create input functions:
    training_input_fn = tfe.estimator.inputs.pandas_input_fn(
        x=X_train,
        y=y_train,
        batch_size=BATCH_SIZE,
        num_epochs=None,
        shuffle=True
    )
    test_input_fn = tfe.estimator.inputs.pandas_input_fn(
        x=X_test,
        y=y_test,
        batch_size=BATCH_SIZE,
        num_epochs=1,
        shuffle=True
    )

    return input_features, target.unique().tolist(), training_input_fn, test_input_fn


def test_dnn_classifier():
    # clean model directory from previous run
    print(os.getcwd())
    model_dir_path = os.path.join(os.getcwd(), MODEL_DIR)
    for file in os.listdir(model_dir_path):
        file_path = os.path.join(model_dir_path, file)
        if os.path.isfile(file_path):
            os.remove(file_path)

    # shutil.rmtree(model_dir_path)
    # os.rmdir(model_dir_path)

    # get data
    input_features, target_labels, training_input_fn, test_input_fn = prepare_startups_dataset()

    # instantiate model
    config = tfe.estimator.RunConfig(
        log_step_count_steps=1,
        save_summary_steps=1,
    )
    categorical_classifier = tfe.estimator.DNNClassifier(hidden_units=[50, 40],
                                                         feature_columns=input_features,
                                                         label_vocabulary=target_labels,
                                                         model_dir=model_dir_path,
                                                         n_classes=len(target_labels),
                                                         optimizer=tf.train.AdamOptimizer(learning_rate=0.00025),
                                                         batch_norm=False,  # better results without batchnorm
                                                         dropout=0.3,
                                                         config=config)
    # train

    categorical_classifier.train(input_fn=training_input_fn, steps=EPOCHS)

    # evaluate
    results = categorical_classifier.evaluate(input_fn=test_input_fn)
    print(results)


if __name__ == '__main__':
    print(tf.VERSION)
    print(tf.keras.__version__)
    tf.random.set_random_seed(RANDOM_SEED)

    logging.getLogger().setLevel(logging.INFO)
    test_dnn_classifier()
