#### TensorFlow projects
This package contains sample projects for learning TensorFlow.  


####List of projects:

#####estimator-test
* project for testing tf.estimator.DNNClassifier and Estimator API in general
* uses the 'Kikstarter Projects' dataset (https://www.kaggle.com/kemical/kickstarter-projects#ks-projects-201801.csv)
* estimator-test - simple project for testing TensorFlow Estimator API
* TensorBoard-test - project for testing Dataset API and TensorBoard usage


####Useful links:
* https://medium.com/learning-machine-learning/introduction-to-tensorflow-estimators-part-1-39f9eb666bc7
* https://storage.googleapis.com/pub-tools-public-publication-data/pdf/18d86099a350df93f2bd88587c0ec6d118cc98e7.pdf
* https://medium.com/@anthony_sarkis/tensorboard-quick-start-in-5-minutes-e3ec69f673af
