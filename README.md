# Projects for machine learning practice
Directories and projects:
* GAN - simple GAN network
* TensorFlow - scripts for learning TensorFlow

#### Starting TensorBoard on Windows
```
tensorboard --logdir <model_dir>
```
Fix until TensorBoard 1.13.1 is release: https://stackoverflow.com/questions/54814113/invalid-format-string-tensorboard

#### Setting up new venv:
```
virtualenv -p python ./venv
.\venv\Scripts\activate
pip install --upgrade pip
deactivate
```

#### nvidia-smi on windows:
```
<go to C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe>
while (1){.\nvidia-smi.exe; sleep 1}
```